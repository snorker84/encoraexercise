import BugSearch from './components/bug/BugSearch';
import './App.css';

function App() {
  return (
    <div className="App">
      <div className="bug-search-container">
        <BugSearch />
      </div>
    </div>
  );
}

export default App;
