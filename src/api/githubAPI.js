const BASE_API = 'https://api.github.com/';
const REPO = 'facebook/react';

const getIssues = async (key) => {
    const query = await fetch(`${BASE_API}search/issues?q=repo:${REPO}+${key}:in:title`);
    const data = await query.json();
    return data;
}


export const githubAPI = {
    getIssues
}