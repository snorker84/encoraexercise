
import React, { useEffect, useState } from 'react';
import { githubAPI } from '../../api/githubAPI';

export default function BugSearch() {

    const [bugData, setBugData] = useState(null);
    const [searchValue, setSearchValue] = useState("");
    const [triggerCall, setTriggerCall] = useState(true);

    const onUpdateSearchValue = (event) => {
        setSearchValue(event.target.value);
        if (triggerCall) {
            async function makeCall() {
                await timeout(1000);
                setTriggerCall(true);
            }
            setTriggerCall(false);
            makeCall();

        }
    }

    const timeout = (ms) => {
        return new Promise(resolve => setTimeout(resolve, ms));
    }

    useEffect(() => {
        if(triggerCall) {
            githubAPI.getIssues(searchValue).then((data) => {
                setBugData(data);
            });
        }
    }, [triggerCall]);

    return(
        <div id='bug-search-component'>
            <div id='input-bug-search'>
                <input type='text' placeholder='Search'onChange={onUpdateSearchValue}/>
            </div>
            { bugData && 
                bugData.items.map((item) => {
                    return(
                        <div key={item.id} className='bug-list-item'>
                            {item.title}
                        </div>
                    )
                })
            }
        </div>
    );
}